#!/usr/bin/env bash
PASSWORD='admin123'

sudo apt-get update

echo "Installing Apache.."

sudo apt-get install -y apache2

#echo "Installing Tomcat.."

#sudo apt-get install -y tomcat7

#echo "Installing Tomcat7 docs.."

#sudo apt-get install -y tomcat7-docs

#echo "Installing Tomcat7 administration webapps.."

#sudo apt-get install -y tomcat7-admin

#echo "Installing Tomcat7 examples webapps.."

#sudo apt-get install tomcat7-examples

echo "Installing Git.."

sudo apt-get install -y git

echo "Installing Maven.."

sudo apt-get install -y maven

echo "Installing Java 8.."

#sudo apt-get install -y software-properties-common python-software-properties

echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | sudo /usr/bin/debconf-set-selections

sudo add-apt-repository ppa:webupd8team/java -y

sudo apt-get update

sudo apt-get install oracle-java8-installer

echo "Setting environment variables for Java 8.."

sudo apt-get install -y oracle-java8-set-default

echo "\n----- Installing Tomcat ------\n"
sudo apt-get -y install tomcat8
## Install Tomcat 8 Web Admin
sudo apt-get -y install tomcat8-admin
## Install Tomcat 8 Documentation and Examples
sudo apt-get -y install tomcat8-docs tomcat8-examples

echo "\n----- Installing MySQL server ------\n"
# install mysql and give password to installer
sudo debconf-set-selections <<< "mysql-server mysql-server/root_password password $PASSWORD"
sudo debconf-set-selections <<< "mysql-server mysql-server/root_password_again password $PASSWORD"
sudo apt-get -y install mysql-server
sudo sed -i '/bind-address/c\#bind-address = 127.0.0.1' /etc/mysql/mysql.conf.d/mysqld.cnf
sudo sed -i '/skip-external-locking/c\#skip-external-locking' /etc/mysql/mysql.conf.d/mysqld.cnf
echo "\n----- Grant Privileges to root user ------\n"
mysql -u root -padmin123 <<'EOF'
use mysql
GRANT ALL ON *.* to root@'%' IDENTIFIED BY 'admin123'; 
FLUSH PRIVILEGES; 
exit
EOF
sudo service mysql restart